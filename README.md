<h1>Koch Curve</h1>

<p>
When I was first introduced to the Mandelbrot set through a PBS documentary
(roughly around the age of 14), I was fascinated by the sheer simplicity,
complexity, and beauty of fractal geometry and self-similarity. For this
phenomenon cannot only be observed universally throughout nature, however,
we now have a mathematical bases for this phenomenon. The Koch Curve is
essentially a simple illustration of this concept, by iteratively dividing
itself into ever-increasing segments of their previous derived segment.
</p>

<h1>Live Demo</h1>

<p>
<a href="http://byrne-systems.com/koch-curve/">http://byrne-systems.com/koch-curve/</a>
</p>
