/* Set: Canvas's Initial Width */
window.addEventListener('load', eventWindowLoaded, false);

function eventWindowLoaded() { canvasApp(); }

function canvasSupport() { return Modernizr.canvas; }

function canvasApp() {

    // if: no canvas support return, else declare 2D canvas
    if (!canvasSupport()) { return; } else {
        var theCanvas = document.getElementById('cvs');
        var context = theCanvas.getContext('2d');
    }

    // Set: line Length to be used; in pixels
    var lineLength = 10;

    // Define: DOM Window Object
    var domWindow = {
        width: window.innerWidth - 18,
        xCenter: (window.innerWidth / 2) - (lineLength / 2),
        height: window.innerHeight - 4,
        yCenter: (window.innerHeight / 2) - (lineLength / 2)
    };

    // Set: Canvas Dimensions
    theCanvas.width = domWindow.width;
    theCanvas.height = domWindow.height;

    var deg = Math.PI/180;                                  // Converting degrees to radians
    var length = 500;                                       // Line length

    // Draw a level-n Koch Snowflake fractal in the context
    // with lower-left corner at (x,y) and side length len.
    function snowflake(c, n, x, y, len) {
        c.save()                                            // Save current transformation
        c.translate(x,y);                                   // Translate to starting point
        c.beginPath();                                      // Start new Path
        c.moveTo(0,0);                                      // Begin a new subpath there
        leg(n);                                             // Draw the first leg of the fractal
        c.rotate(-120*deg);                                 // Rotate 120 degrees anticlockwise
        leg(n);                                             // Draw the second leg
        c.rotate(-120*deg);                                 // Rotate 120 degrees anticlockwise
        leg(n);                                             // Draw the final leg
        c.closePath();                                      // Close the subpath
        c.restore();                                        // Restore Original transformation

        // Draw a single leg of a level-n Koch snowflake.
        // This function leaves the current point at the end of
        // the leg it has drawn and translates the coordinate
        // system so the current point is (0,0). This means you
        // can easily call rotate() after drawing a leg.
        function leg(n) {
            c.save();                                       // Save current transform
            if (n == 0) {                                   // Non-recursive case:
                c.lineTo(len,0);                            //   Just a horizontal line
            } else {                                        // Recursive case:        _  _
                                                            //   draw 4 sub-legs like: \/
                c.scale(1/3,1/3);                           // sub-legs are 1/3rd size
                leg(n-1);                                   // Draw the first sub-leg
                c.rotate(60*deg);                           // Turn 60 degrees clockwise
                leg(n-1);                                   // Draw the second sub-leg
                c.rotate(-120*deg);                         // Rotate 120 degrees back
                leg(n-1);                                   // Third sub-leg
                c.rotate(60*deg);                           // Back to original heading
                leg(n-1);                                   // Final sub-leg
            }

        c.restore();                                        // Restore the transform
        c.translate(len,0);                                 // Translate to end of leg

        }
    }

    function writeIteration(i) {
        var index = i + 1;
        context.font = '20px Arial';
        context.fillStyle = 'black';
        context.fillText('Iteration: ' + index,domWindow.xCenter-40,domWindow.yCenter-10);
    }

    var i = 0;
    context.strokeStyle = 'blue';

    setInterval(function() {
        if (i > 6) i = 0;
        context.fillStyle = 'green';
        context.clearRect(0,0,domWindow.width,domWindow.height);
        snowflake(context,i,domWindow.xCenter-(length*0.5),domWindow.yCenter+(length*0.29),length);
        context.stroke();
        context.fill();
        writeIteration(i);
        i++;
    },1000);
}